import java.util.ArrayList;
import java.util.List;

public class StringCalculator {

    public static int add(final String numbers) {
        String delimiter = ",|;|n|/|%|[*]|\\[|]";
        String numbersWithoutDelimiter = numbers;
        if (numbers.startsWith("//")) {
            int startDelimiterIndex = 2;
            int endDelimiterIndex = numbers.indexOf("//") + startDelimiterIndex;
            delimiter = numbers.substring(endDelimiterIndex, endDelimiterIndex + 1);
            numbersWithoutDelimiter = numbers.substring(numbers.indexOf("n") + 1);
        }
        return add(numbersWithoutDelimiter, delimiter);
    }

    private static int add(final String numbers, final String delimiter) {
        int returnValue = 0;
        final int oneThousand = 1000;
        String[] numbersArray = numbers.split(",|;|n|/|%|[*]|\\[|]");
        List<Integer> negativeNumbers = new ArrayList<Integer>();
        for (String number : numbersArray) {
            if (!number.trim().isEmpty()) {
                int numberInt = Integer.parseInt(number.trim());
                if (numberInt < 0) {
                    negativeNumbers.add(numberInt);
                }
                else if (numberInt <= oneThousand) {
                    returnValue += numberInt;
                }
            }
        }
        if (negativeNumbers.size() > 0) {
            throw new RuntimeException("Negatives not allowed: " + negativeNumbers.toString());
        }
        return returnValue;
    }
}

import org.junit.Assert;
import org.junit.Test;

public class StringCalculatorTest {

//    @Test(expected = RuntimeException.class)
//    public void whenMoreThan2NumbersAreUsedThenExceptionIsThrown() {
//        StringCalculator.add("1,2,3");
//    }

    @Test
    public void anyNumberInNumbersReturnTheirSum() {
        Assert.assertEquals(5+8+25+17+43+39, StringCalculator.add("5,8,25,17,43,39"));
    }

    @Test
    public void twoNumbersAreTrue() {
        StringCalculator.add("4,5");
        Assert.assertTrue(true);
    }

    @Test(expected = RuntimeException.class)
    public void ifOneOfNumbersAreNotNumbersGetException() {
        StringCalculator.add("2,X");
    }

    @Test
    public void emptyStringReturn0() {
        Assert.assertEquals(0, StringCalculator.add(""));
    }

    @Test
    public void oneNumberReturnThisNumber() {
        Assert.assertEquals(5, StringCalculator.add("5"));
    }

    @Test
    public void twoNumbersReturnSum() {
        Assert.assertEquals(7+9, StringCalculator.add("7,9"));
    }

    @Test
    public void newLineBetweenNumbersReturnSum() {
        Assert.assertEquals(4+8+17, StringCalculator.add("4n8,17"));
    }

    @Test
    public void delimiterIsSpecifiedToSeparateNumbers() {
        Assert.assertEquals(2+8+35, StringCalculator.add("//;n2;8;35"));
    }

    @Test(expected = RuntimeException.class)
    public void negativeNumbersRuntimeException() {
        StringCalculator.add("4,7,14,-19,43,38");
    }

    @Test
    public void negativeNumbersRuntimeExceptionGetMessage() {
        RuntimeException exception = null;
        try {
            StringCalculator.add("1,5,-11,15,-37,43");
        } catch (RuntimeException e) {
            exception = e;
        }
        Assert.assertNotNull(exception);
        Assert.assertEquals("Negatives not allowed: [-11, -37]", exception.getMessage());
    }

    @Test
    public void oneOfNumbersBiggerThan1000NotIncludedInSum() {
        Assert.assertEquals(5+1000+9, StringCalculator.add("5,1000,1001,9,1347"));
    }

    @Test
    public void delimiterHasPercentOrStarToSeparateNumbers() {
        Assert.assertEquals(5+8+10, StringCalculator.add("%\n**5**%**8%10*****"));
    }

    @Test
    public void delimiterCanBeAnyLength(){
        Assert.assertEquals(6, StringCalculator.add("//[*][%]\n1*2%3"));
    }

    @Test
    public void allowMultipleDelimiter(){
        Assert.assertEquals(6, StringCalculator.add("//[***][%%]\n1***2%%3"));
    }
}